<?php

namespace Database\Factories;

use App\Models\Parents;
use Illuminate\Database\Eloquent\Factories\Factory;

class ParentsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model= Parents::class;
    public function definition()
    {
        return [
            'fullname' => $this->faker->name,
            'email' => $this->faker->email,
            'mobile' => $this->faker->phoneNumber,
            'password' => $this->faker->password,
            'created_at' => $this->faker->dateTime,
        ];
    }
}
