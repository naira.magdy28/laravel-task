<?php



namespace App\Http\Controllers;

use Illuminate\Http\Request;



class ApiController extends Controller

{
    public function __construct(MethodsController $methods)

    {

        $this->methods = $methods;

    }

    //get token

    public function getToken(Request $request)

    {
        $this->methods->saveLog($request->method(), json_encode($request->all()), $request->fullUrl(), 'active', true);

        return $this->methods->token();

    }

    //login
    public function login(Request $request)

    {

//        $email = $request->input('email');
//
//        $password = $request->input('password');
        $name = $request->input('fullname');


        return $this->methods->login($name);

    }

    //show my partner
    public function show_my_partner(Request $request)

    {
        return $this->methods->show_my_partner();
    }


    //add partner
    public function add_partner(Request $request)

    {
        $partner_id = $request->input('partner_id');
        return $this->methods->add_partner($partner_id);
    }

    //add new baby
    public function add_new_baby(Request $request)

    {
        $name = $request->input('name');
        $age = $request->input('age');
        $gender = $request->input('gender');
        return $this->methods->add_new_baby($name,$age,$gender);
    }

    //edit baby
    public function edit_baby(Request $request)

    {
        return $this->methods->edit_baby();
    }

    //show baby
    public function show_baby($id)

    {
        $user=getUser();
        $conditions = [
            'id' => $id,
            'parent_id' =>$user->id
        ];
        $selections = ['*'];

        return $this->methods->show_baby($conditions, $selections);

    }
    //show all babies
    public function show_all_babies()

    {
        $user=getUser();
        $conditions = [
            'parent_id' =>$user->id
        ];
        $selections = ['*'];

        return $this->methods->show_all_babies($conditions, $selections);

    }

    //delete baby
    public function delete_baby(Request $request)

    {
        return $this->methods->delete_baby();
    }
}
