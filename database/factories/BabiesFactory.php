<?php

namespace Database\Factories;

use App\Models\Parents;
use Illuminate\Database\Eloquent\Factories\Factory;

class BabiesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $gender = $this->faker->randomElement(['male', 'female']);

        return [

            'parent_id'=>Parents::inRandomOrder()->first()->id,
            'name'=> $this->faker->name($gender),
            'age'=> $this->faker->numberBetween($min = 1, $max = 10),
            'gender'=> $gender,
            'created_at' => $this->faker->dateTime,

        ];
    }
}
