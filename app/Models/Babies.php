<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Babies extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'babies';

    protected $fillable = [
        'parent_id',
        'name',
        'age',
        'gender'
    ];

}
