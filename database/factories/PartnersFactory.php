<?php

namespace Database\Factories;

use App\Models\Parents;
use Illuminate\Database\Eloquent\Factories\Factory;

class PartnersFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'parent_id'=>Parents::inRandomOrder()->first()->id,
            'partner_id'=>Parents::inRandomOrder()->first()->id,
        ];
    }
}
