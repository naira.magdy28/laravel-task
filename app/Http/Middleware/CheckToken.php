<?php



namespace App\Http\Middleware;





use App\Models\ApiLog;
use Closure;
use Illuminate\Support\Facades\DB;


class CheckToken

{

    /**

     * Handle an incoming request.

     *

     * @param  \Illuminate\Http\Request $request

     * @param  \Closure $next

     * @return mixed

     */

    public function handle($request, Closure $next)

    {





        $token = DB::table('api_token')->first();


            if (request()->header('API-TOKEN') ===$token->token || $token->token == $request->token) {

            $user = getUser();
            if($user){

                $request->request->add(['UserLogin' => $user]);

            }

            $log = new ApiLog();

            $log->method = $request->method();

            $log->params = json_encode($request->all());

            $log->url = $request->fullUrl();

            $log->message = '';

            $log->success = true;

            $log->createdtime = date("Y-m-d H:i:s");

            $log->save();

            return $next($request)->header('Access-Control-Allow-Origin', '*')->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

        }

        $log = new ApiLog();

        $log->method = $request->method();

        $log->params = json_encode($request->all());

        $log->url = $request->fullUrl();

        $log->message = 'Invalid Token';

        $log->success = false;

        $log->createdtime = date("Y-m-d H:i:s");

        $log->save();

        return response()->json(['message' => 'token_expired', 'success' => false]);

    }

}

