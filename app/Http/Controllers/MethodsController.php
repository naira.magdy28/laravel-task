<?php


namespace App\Http\Controllers;

use App\Models\ApiLog;
use App\Models\Babies;
use App\Models\Parents;
use App\Models\Partners;
use http\Env\Request;
use Illuminate\Support\Facades\DB;

class MethodsController extends Controller

{

    // api_log
    public function saveLog($method, $params, $url, $message, $success)
    {
        $log = new ApiLog();
        $log->method = $method;
        $log->params = $params;
        $log->url = $url;
        $log->message = $message;
        $log->success = $success;
        $log->createdtime = date("Y-m-d H:i:s");
        $log->save();
    }

    //get token
    public function token()
    {
        $token = DB::table('api_token')->first();
        $data['message'] = 'active';
        $data['success'] = true;
        $data['result'] = $token->token;
        return $this->response($data);
    }

    //login

    //login
    public function login($name)
    {
        $user = Parents::where('fullname', '=', $name)->first();
//        $user = Parents::where('email', '=', $email)->where('password', '=', $password)->first();
        if ($user) {
            $data['message'] = 'تم تسجيل الدخول بنجاح';
            $data['success'] = true;
            $data['result'] = $user;
        } else {
            $data['message'] = 'برجاء التأكد من بيانات التسجيل';
            $data['success'] = false;
            $data['result'] = null;
        }
        return $this->response($data);
    }

    //show_my_partner
    public function show_my_partner(){
        $user = getUser();
        if ($user) {
         $partner=Parents::select('parents.*')->leftJoin('parents_partners','partner_id','parents.id')->where('parent_id',$user->id)->first();
         if($partner){
             $data['message'] = 'partner found';
             $data['success'] = true;
             $data['result'] = $partner;
         }
         else{
             $data['message'] = 'no partner found';
             $data['success'] = false;
             $data['result'] = null;
         }
        }
        else{
            $data['message'] = 'no user found';
            $data['success'] = false;
            $data['result'] = null;
        }

        return $this->response($data);
    }
  //add partner
    public function add_partner($partner_id){
        $user = getUser();
        if ($user) {
            $partner_data=Parents::where('id','=',$partner_id)->where('id','!=',$user->id)->first();
            if($partner_data){
                $partner=new Partners();
                $partner->parent_id=$user->id;
                $partner->partner_id=$partner_data->id;
                $partner->save();
                if($partner->id){
                    $data['message'] = 'Partner Added Successfully';
                    $data['success'] = true;
                    $data['result'] = $partner;
                }
                else{
                    $data['message'] = 'Something went wrong';
                    $data['success'] = false;
                    $data['result'] = null;
                }
            }else{
                if($user->id == $partner_id){
                    $data['message'] = 'Cannot choose your self as a partner';
                    $data['success'] = false;
                    $data['result'] = null;
                }
                else{
                    $data['message'] = 'No Partner Found';
                    $data['success'] = false;
                    $data['result'] = null;
                }

            }

        }
        else{
            $data['message'] = 'no user found';
            $data['success'] = false;
            $data['result'] = null;
        }

        return $this->response($data);
    }


 //add new baby
    public function add_new_baby($name,$age,$gender){
        $user = getUser();
        if ($user) {
         $baby=new Babies();
            $baby->parent_id=$user->id;
            $baby->name=$name;
            $baby->age=$age;
            $baby->gender=$gender;
            $baby->created_at=now();
            $baby->save();
         if($baby->id){
             $data['message'] = 'Baby Added Successfully';
             $data['success'] = true;
             $data['result'] = $baby;
         }
         else{
             $data['message'] = 'Something went wrong';
             $data['success'] = false;
             $data['result'] = null;
         }
        }
        else{
            $data['message'] = 'no user found';
            $data['success'] = false;
            $data['result'] = null;
        }

        return $this->response($data);
    }

 //edit baby
    public function edit_baby(){
        $request=Request();
        $user = getUser();
        if ($user) {
         $baby=Babies::where('id','=',$request->id)->where('parent_id','=',$user->id)->first();
            $baby->name=$request->name;
            $baby->age=$request->age;
            $baby->gender=$request->gender;
         if($baby->save()){
             $data['message'] = 'Baby Edited Successfully';
             $data['success'] = true;
             $data['result'] = $baby;
         }
         else{
             $data['message'] = 'Something went wrong';
             $data['success'] = false;
             $data['result'] = null;
         }
        }
        else{
            $data['message'] = 'no user found';
            $data['success'] = false;
            $data['result'] = null;
        }

        return $this->response($data);
    }

    //show baby

    public function show_baby($conditions, $selections)
    {
        $baby = Babies::select(array_values($selections));
        foreach ($conditions as $key => $val) {
            $baby = $baby->where($key, '=', $val);
        }
        $baby = $baby->first();
        if($baby){
            $data['message'] = '';
            $data['success'] = true;
            $data['result'] = $baby;
        }else{

            $data['message'] = 'No baby Found';
            $data['success'] = false;
            $data['result'] = null;
        }
        return $this->response($data);

    }

    //show all babies

    public function show_all_babies($conditions, $selections)
    {
        $baby = Babies::select(array_values($selections));
        foreach ($conditions as $key => $val) {
            $baby = $baby->where($key, '=', $val);
        }
        $baby = $baby->get();
        if($baby){
            $data['message'] = '';
            $data['success'] = true;
            $data['result'] =(object)$baby;
        }else{

            $data['message'] = 'No babies Found';
            $data['success'] = false;
            $data['result'] = null;
        }
        return $this->response($data);
    }


    //delete baby
    public function delete_baby(){
        $request=Request();
        $user = getUser();
        if ($user) {
            $baby=Babies::where('id','=',$request->id)->where('parent_id','=',$user->id)->first();
            if($baby){
                if($baby->delete()){
                    $data['message'] = 'Baby Deleted Successfully';
                    $data['success'] = true;
                    $data['result'] = $baby;
                }
                else{
                    $data['message'] = 'Something went wrong';
                    $data['success'] = false;
                    $data['result'] = null;
                }
            }else{
                $data['message'] = 'No baby Found';
                $data['success'] = false;
                $data['result'] = null;
            }

        }
        else{
            $data['message'] = 'no user found';
            $data['success'] = false;
            $data['result'] = null;
        }

        return $this->response($data);
    }


    private function response($data)
    {
        $user = getUser();
        if ($user) {
            $data['successLogin'] = true;
        } else {
            $data['successLogin'] = false;
        }
        return response()->json($data)
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Content-Type', 'application/json;charset=UTF-8')
            ->header('Access-Control-Allow-Methods', 'POST, GET, PATCH, PUT')
            ->header('Charset', 'utf-8');
    }

}
