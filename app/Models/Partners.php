<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Partners extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $table = 'parents_partners';

    protected $fillable = [
        'parent_id',
        'partner_id',
    ];

}
