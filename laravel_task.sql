-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2022 at 02:38 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_log`
--

CREATE TABLE `api_log` (
  `id` int(11) NOT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `success` tinyint(1) DEFAULT NULL,
  `createdtime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `api_log`
--

INSERT INTO `api_log` (`id`, `method`, `params`, `url`, `message`, `success`, `createdtime`) VALUES
(1, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n', 'active', 1, '2022-07-28 06:54:59'),
(2, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n', 'active', 1, '2022-07-28 06:55:29'),
(3, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n', 'active', 1, '2022-07-28 06:55:43'),
(4, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n', 'Invalid Token', 0, '2022-07-28 07:00:16'),
(5, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"Route::get(\'get_token\', [ApiController::class, \'getToken\']);\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=Route%3A%3Aget%28%27get_token%27%2C%20%5BApiController%3A%3Aclass%2C%20%27getToken%27%5D%29%3B', 'Invalid Token', 0, '2022-07-28 07:00:28'),
(6, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\\\",\\\"successLogin\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c%22%2C%22successLogin', 'Invalid Token', 0, '2022-07-28 07:00:44'),
(7, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\\\",\\\"successLogin\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c%22%2C%22successLogin', 'Invalid Token', 0, '2022-07-28 07:00:48'),
(8, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"api_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\\\",\\\"successLogin\"}', 'http://127.0.0.1:8000/api/get_token?api_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c%22%2C%22successLogin&email=witting.danyka%40hudson.com&password=Ml~d_k7n', 'Invalid Token', 0, '2022-07-28 07:00:54'),
(9, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\\\",\\\"successLogin\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c%22%2C%22successLogin', 'Invalid Token', 0, '2022-07-28 07:01:58'),
(10, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\\\",\\\"successLogin\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c%22%2C%22successLogin', 'Invalid Token', 0, '2022-07-28 07:03:18'),
(11, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 07:06:26'),
(12, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/get_token?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', 'active', 1, '2022-07-28 07:06:27'),
(13, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:13:35'),
(14, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:13:45'),
(15, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:14:24'),
(16, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:14:53'),
(17, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:15:17'),
(18, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:15:50'),
(19, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:16:14'),
(20, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:16:17'),
(21, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:16:34'),
(22, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:17:05'),
(23, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', '', 1, '2022-07-28 17:17:29'),
(24, 'POST', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/login', 'Invalid Token', 0, '2022-07-28 20:02:23'),
(25, 'POST', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/login', 'Invalid Token', 0, '2022-07-28 20:02:43'),
(26, 'POST', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/login', 'Invalid Token', 0, '2022-07-28 20:02:55'),
(27, 'POST', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/login', 'Invalid Token', 0, '2022-07-28 20:08:09'),
(28, 'POST', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/login', '', 1, '2022-07-28 20:08:29'),
(29, 'GET', '[]', 'http://127.0.0.1:8000/api/show_my_partner', '', 1, '2022-07-28 20:10:29'),
(30, 'GET', '[]', 'http://127.0.0.1:8000/api/show_my_partner', '', 1, '2022-07-28 20:11:41'),
(31, 'GET', '[]', 'http://127.0.0.1:8000/api/show_my_partner', '', 1, '2022-07-28 20:12:11'),
(32, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n', '', 1, '2022-07-28 20:13:42'),
(33, 'POST', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/login', '', 1, '2022-07-28 20:22:50'),
(34, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n', '', 1, '2022-07-28 20:23:23'),
(35, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n', '', 1, '2022-07-28 20:30:22'),
(36, 'POST', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/login', '', 1, '2022-07-28 20:30:27'),
(37, 'GET', '{\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_my_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n', '', 1, '2022-07-28 20:30:46'),
(38, 'POST', '{\"partner_id\":\"17\"}', 'http://127.0.0.1:8000/api/add_partner', '', 1, '2022-07-28 20:32:30'),
(39, 'POST', '{\"partner_id\":\"17\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/add_partner?email=witting.danyka%40hudson.com&password=Ml~d_k7n', '', 1, '2022-07-28 20:32:59'),
(40, 'POST', '{\"name\":\"joy\",\"age\":\"10\",\"gender\":\"male\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"email\":\"witting.danyka@hudson.com\",\"password\":\"Ml~d_k7n\"}', 'http://127.0.0.1:8000/api/add_new_baby?email=witting.danyka%40hudson.com&password=Ml~d_k7n', '', 1, '2022-07-28 20:39:41'),
(41, 'POST', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/login', '', 1, '2022-07-28 21:47:21'),
(42, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_my_partner?fullname=Larry%20Mills', '', 1, '2022-07-28 21:47:49'),
(43, 'POST', '{\"partner_id\":\"1\",\"Larry_Mills\":null}', 'http://127.0.0.1:8000/api/add_partner?Larry%20Mills=', '', 1, '2022-07-28 21:52:06'),
(44, 'POST', '{\"partner_id\":\"1\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/add_partner?fullname=Larry%20Mills', '', 1, '2022-07-28 21:52:20'),
(45, 'POST', '{\"partner_id\":\"1\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/add_partner?fullname=Larry%20Mills', '', 1, '2022-07-28 21:52:56'),
(46, 'POST', '{\"partner_id\":\"20\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/add_partner?fullname=Larry%20Mills', '', 1, '2022-07-28 21:53:07'),
(47, 'POST', '{\"partner_id\":\"20\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/add_partner?fullname=Larry%20Mills', '', 1, '2022-07-28 21:54:42'),
(48, 'POST', '{\"partner_id\":\"11\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/add_partner?fullname=Larry%20Mills', '', 1, '2022-07-28 21:54:53'),
(49, 'POST', '{\"partner_id\":\"11\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/add_partner?fullname=Larry%20Mills', '', 1, '2022-07-28 21:55:10'),
(50, 'POST', '{\"partner_id\":\"1\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/add_partner?fullname=Larry%20Mills', '', 1, '2022-07-28 21:55:28'),
(51, 'POST', '{\"partner_id\":\"25\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/add_partner?fullname=Larry%20Mills', '', 1, '2022-07-28 21:55:40'),
(52, 'POST', '{\"name\":\"jasmine\",\"age\":\"5\",\"gender\":\"female\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/add_new_baby?fullname=Larry%20Mills', '', 1, '2022-07-28 21:56:34'),
(53, 'POST', '{\"name\":\"jasmine edited name\",\"age\":\"5\",\"gender\":\"female\",\"id\":\"22\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/add_new_baby?fullname=Larry%20Mills', '', 1, '2022-07-28 22:06:28'),
(54, 'POST', '{\"name\":\"jasmine edited name\",\"age\":\"5\",\"gender\":\"female\",\"id\":\"23\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/edit_baby?fullname=Larry%20Mills', '', 1, '2022-07-28 22:07:13'),
(55, 'POST', '{\"name\":\"jasmine edited 2\",\"age\":\"5\",\"gender\":\"female\",\"id\":\"23\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/edit_baby?fullname=Larry%20Mills', '', 1, '2022-07-28 22:07:36'),
(56, 'POST', '{\"name\":\"jasmine edited 5\",\"age\":\"6\",\"gender\":\"female\",\"id\":\"23\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/edit_baby?fullname=Larry%20Mills', '', 1, '2022-07-28 22:08:01'),
(57, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_baby/23?fullname=Larry%20Mills', '', 1, '2022-07-28 22:14:57'),
(58, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_baby/23?fullname=Larry%20Mills', '', 1, '2022-07-28 22:15:29'),
(59, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_baby/23?fullname=Larry%20Mills', '', 1, '2022-07-28 22:19:10'),
(60, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_baby/10?fullname=Larry%20Mills', '', 1, '2022-07-28 22:20:56'),
(61, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_baby/10?fullname=Larry%20Mills', '', 1, '2022-07-28 22:21:33'),
(62, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_baby/10?fullname=Larry%20Mills', '', 1, '2022-07-28 22:22:22'),
(63, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_baby/10?fullname=Larry%20Mills', '', 1, '2022-07-28 22:22:55'),
(64, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_baby/10?fullname=Larry%20Mills', '', 1, '2022-07-28 22:23:12'),
(65, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_baby/23?fullname=Larry%20Mills', '', 1, '2022-07-28 22:23:27'),
(66, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_all_babies?fullname=Larry%20Mills', '', 1, '2022-07-28 22:27:36'),
(67, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_all_babies?fullname=Larry%20Mills', '', 1, '2022-07-28 22:28:17'),
(68, 'POST', '{\"id\":\"20\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/delete_baby?fullname=Larry%20Mills', '', 1, '2022-07-28 22:34:59'),
(69, 'POST', '{\"id\":\"21\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null},\"fullname\":\"Larry Mills\"}', 'http://127.0.0.1:8000/api/delete_baby?fullname=Larry%20Mills', '', 1, '2022-07-28 22:35:39'),
(70, 'GET', '{\"fullname\":\"Larry Mills\",\"UserLogin\":{\"id\":1,\"fullname\":\"Larry Mills\",\"email\":\"witting.danyka@hudson.com\",\"mobile\":\"520-638-3267\",\"password\":\"Ml~d_k7n\",\"created_at\":\"2004-07-22 23:26:35\",\"updated_at\":\"2022-07-28 01:22:26\",\"deleted_at\":null}}', 'http://127.0.0.1:8000/api/show_all_babies?fullname=Larry%20Mills', '', 1, '2022-07-28 22:36:07');

-- --------------------------------------------------------

--
-- Table structure for table `api_token`
--

CREATE TABLE `api_token` (
  `id` int(11) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `api_token`
--

INSERT INTO `api_token` (`id`, `token`) VALUES
(1, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c');

-- --------------------------------------------------------

--
-- Table structure for table `babies`
--

CREATE TABLE `babies` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `gender` enum('male','female','','') NOT NULL DEFAULT 'male',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_At` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `babies`
--

INSERT INTO `babies` (`id`, `parent_id`, `name`, `age`, `gender`, `created_at`, `updated_at`, `deleted_At`) VALUES
(1, 19, 'Connie Kunze', 1, 'female', '2014-01-27 13:15:26', '2022-07-28 16:30:51', NULL),
(2, 20, 'Ashleigh Bednar', 7, 'female', '1985-09-02 06:50:19', '2022-07-28 16:30:51', NULL),
(3, 2, 'Mariane Swaniawski', 8, 'female', '1989-09-25 09:50:15', '2022-07-28 16:30:51', NULL),
(4, 16, 'Noemie Pouros II', 2, 'female', '2021-09-24 16:46:05', '2022-07-28 16:30:52', NULL),
(5, 17, 'Bulah Sauer', 8, 'female', '1973-12-20 12:31:05', '2022-07-28 16:30:52', NULL),
(6, 13, 'Mr. Antwan Legros Jr.', 9, 'male', '2004-05-16 11:49:46', '2022-07-28 16:30:52', NULL),
(7, 3, 'Zechariah Towne Sr.', 8, 'male', '1975-10-02 17:29:24', '2022-07-28 16:30:52', NULL),
(8, 3, 'Laurie Nicolas', 2, 'female', '2007-01-07 05:48:34', '2022-07-28 16:30:52', NULL),
(9, 1, 'Mrs. Lexi Wolff PhD', 6, 'female', '2004-02-12 23:54:58', '2022-07-28 16:30:52', NULL),
(10, 12, 'Mr. Jonas Leannon DVM', 10, 'male', '2014-11-28 03:54:20', '2022-07-28 16:30:52', NULL),
(11, 2, 'Ansley Prosacco', 6, 'male', '1990-12-06 13:27:16', '2022-07-28 16:30:52', NULL),
(12, 20, 'Mr. Matt Durgan', 8, 'male', '2015-10-11 05:43:29', '2022-07-28 16:30:52', NULL),
(13, 4, 'Dr. Eliezer Kihn Sr.', 9, 'male', '2021-08-23 04:59:55', '2022-07-28 16:30:52', NULL),
(14, 18, 'Odie Lindgren IV', 4, 'female', '1997-07-12 15:09:55', '2022-07-28 16:30:52', NULL),
(15, 14, 'Dr. Lucious Klocko', 10, 'male', '1972-07-21 19:43:00', '2022-07-28 16:30:52', NULL),
(16, 19, 'Miss Mertie Dicki', 2, 'female', '1975-01-22 17:31:00', '2022-07-28 16:30:52', NULL),
(17, 19, 'Miss Katharina Kshlerin', 4, 'female', '2015-02-16 11:38:22', '2022-07-28 16:30:53', NULL),
(18, 13, 'Gayle Kemmer', 3, 'male', '2015-05-22 20:19:37', '2022-07-28 16:30:53', NULL),
(19, 5, 'Friedrich Mohr', 5, 'male', '1975-08-02 23:12:53', '2022-07-28 16:30:53', NULL),
(20, 20, 'Seamus Osinski', 2, 'male', '2015-07-21 11:53:34', '2022-07-28 16:30:53', NULL),
(21, 1, 'joy', 10, 'male', '2022-07-28 20:39:41', '2022-07-28 22:35:40', '2022-07-28 22:35:40'),
(22, 1, 'jasmine', 5, 'female', '2022-07-28 21:56:34', '2022-07-28 21:56:34', NULL),
(23, 1, 'jasmine edited 5', 6, 'female', '2022-07-28 22:06:28', '2022-07-28 22:08:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`id`, `fullname`, `email`, `mobile`, `password`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Larry Mills', 'witting.danyka@hudson.com', '520-638-3267', 'Ml~d_k7n', '2004-07-22 21:26:35', '2022-07-27 23:22:26', NULL),
(2, 'Kelli Ebert Jr.', 'jennings.hill@gmail.com', '251-227-0490', 'hmNeJ4Y?OdzKlS}S/', '1995-12-03 21:09:07', '2022-07-27 23:22:27', NULL),
(3, 'Rodrigo Weissnat', 'sadie.cruickshank@hotmail.com', '+1-352-774-4067', 'R_/6+@YZx:dLw;qQoj', '2018-03-10 09:49:11', '2022-07-27 23:22:27', NULL),
(4, 'Janiya Ledner III', 'ian.hartmann@hotmail.com', '+1 (920) 249-4727', 'Ov/.2)', '1976-07-21 09:04:18', '2022-07-27 23:22:27', NULL),
(5, 'Percival Gottlieb', 'dawson.collier@wiza.com', '763.345.0949', ':-=v3c0TYH\\WZ(=6', '1986-03-17 23:14:01', '2022-07-27 23:22:27', NULL),
(6, 'Ollie Stracke V', 'dovie.hammes@hoeger.net', '+12537536822', '5S%OG5x', '1996-09-02 23:57:55', '2022-07-27 23:22:27', NULL),
(7, 'Dr. Candido Langosh', 'laufderhar@yahoo.com', '(603) 436-8708', 'P9&>VE7>cn9]B_N+9~Z', '1998-10-10 14:46:33', '2022-07-27 23:22:27', NULL),
(8, 'Brain Leffler', 'gertrude86@white.info', '(661) 716-3060', '+Uhwl&c3u%#\\T)1', '1977-02-04 07:31:05', '2022-07-27 23:22:27', NULL),
(9, 'Dr. Ed Walker', 'gkonopelski@purdy.com', '+13852974863', ';Dc\"kB|6I', '1987-02-20 23:13:30', '2022-07-27 23:22:27', NULL),
(10, 'Kiara Yundt', 'matteo.kub@mueller.com', '+1.906.633.0831', 'b20%0VS', '2019-08-14 20:55:17', '2022-07-27 23:22:27', NULL),
(11, 'Lexi Wuckert MD', 'botsford.adolph@yahoo.com', '660.372.4806', '(pC~,Y:cpJD(Av4/6u', '2008-09-04 21:45:52', '2022-07-27 23:22:28', NULL),
(12, 'Monty Gottlieb II', 'alexandrea39@yahoo.com', '651-970-0660', 'f:_R^\\qeg`\"9+]', '1995-07-01 23:59:38', '2022-07-27 23:22:28', NULL),
(13, 'Miss Sadie Tillman Sr.', 'flueilwitz@tillman.net', '+1.737.978.5053', 'IHCsE*', '2000-08-06 08:13:11', '2022-07-27 23:22:28', NULL),
(14, 'Miss Adriana Donnelly', 'hheller@marquardt.com', '+1 (351) 661-0793', '38?{q2G)@8', '2007-01-21 13:29:17', '2022-07-27 23:22:28', NULL),
(15, 'Gregoria Heaney', 'dovie35@mraz.com', '+1-336-332-1315', '-\"1tQ1Ya<WmF', '1980-07-27 03:48:34', '2022-07-27 23:22:28', NULL),
(16, 'Meda Kiehn Jr.', 'emmerich.rosanna@boyle.com', '847-752-2580', 'hL^`{JE_{-<JCc', '1986-04-04 19:30:39', '2022-07-27 23:22:28', NULL),
(17, 'Dr. Vivian Labadie DDS', 'brenden.cormier@yahoo.com', '+1-580-675-8582', '+6G.[u$', '1983-08-13 17:31:34', '2022-07-27 23:22:28', NULL),
(18, 'Mr. Wilbert Jones DDS', 'xlesch@fisher.com', '563.941.5563', '`HK3u-}C}', '2002-10-10 08:13:04', '2022-07-27 23:22:28', NULL),
(19, 'Reynold Abernathy Sr.', 'skuphal@quitzon.com', '1-650-810-8260', '[2+dmG{ZXW&vB5d0W', '1986-12-08 21:28:10', '2022-07-27 23:22:28', NULL),
(20, 'Kariane Ratke II', 'medhurst.dakota@yahoo.com', '+1 (657) 381-3910', 'X+jCZ=<R}JI0cY', '2007-08-05 02:37:58', '2022-07-27 23:22:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `parents_partners`
--

CREATE TABLE `parents_partners` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `parents_partners`
--

INSERT INTO `parents_partners` (`id`, `parent_id`, `partner_id`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1, 4, 20, '2022-07-28 16:42:19', NULL, '2022-07-28 18:42:19'),
(2, 9, 2, '2022-07-28 16:42:19', NULL, '2022-07-28 18:42:19'),
(3, 1, 12, '2022-07-28 16:42:19', NULL, '2022-07-28 18:42:19'),
(4, 19, 15, '2022-07-28 16:42:19', NULL, '2022-07-28 18:42:19'),
(5, 18, 14, '2022-07-28 18:48:12', NULL, '2022-07-28 20:48:12'),
(6, 13, 10, '2022-07-28 18:44:16', NULL, '2022-07-28 20:44:16'),
(7, 3, 8, '2022-07-28 16:42:19', NULL, '2022-07-28 18:42:19'),
(8, 5, 16, '2022-07-28 18:48:21', NULL, '2022-07-28 20:48:21'),
(9, 6, 7, '2022-07-28 18:46:03', NULL, '2022-07-28 20:46:03'),
(11, 1, 17, '2022-07-28 20:32:59', NULL, '2022-07-28 22:32:59'),
(12, 1, 11, '2022-07-28 21:55:11', NULL, '2022-07-28 23:55:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_log`
--
ALTER TABLE `api_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_token`
--
ALTER TABLE `api_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `babies`
--
ALTER TABLE `babies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fullname` (`fullname`);

--
-- Indexes for table `parents_partners`
--
ALTER TABLE `parents_partners`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_log`
--
ALTER TABLE `api_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `api_token`
--
ALTER TABLE `api_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `babies`
--
ALTER TABLE `babies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `parents_partners`
--
ALTER TABLE `parents_partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
