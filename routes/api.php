<?php

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


    Route::get('get_token', [ApiController::class, 'getToken']);

    Route::group(['middleware' => 'checkToken'], function () {

        Route::post('login', [ApiController::class, 'login']);
        Route::get('show_my_partner', [ApiController::class, 'show_my_partner']);
        Route::post('add_partner', [ApiController::class, 'add_partner']);
        Route::post('add_new_baby', [ApiController::class, 'add_new_baby']);
        Route::post('edit_baby', [ApiController::class, 'edit_baby']);
        Route::get('show_baby/{id}', [ApiController::class, 'show_baby']);
        Route::get('show_all_babies', [ApiController::class, 'show_all_babies']);
        Route::post('delete_baby', [ApiController::class, 'delete_baby']);


    });

