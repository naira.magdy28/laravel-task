<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Parents extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $table = 'parents';

    protected $fillable = [
        'fullname',
        'email',
        'mobile',
        'password'
    ];

    protected $hidden = [
        'password'
    ];


    public function babies(){

        return $this->hasmany('App\OurPartnersFlags','id','parent_id');

    }


}
